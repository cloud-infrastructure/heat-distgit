FROM docker.io/openstackhelm/heat:train-ubuntu_bionic

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
        apt-get install -y \
        patch \
        wget && \
        apt-get clean

# install heat with CERN patches
COPY . /code
RUN mkdir /tar && \
    cd /tar && \
    wget https://tarballs.opendev.org/openstack/heat/openstack-heat-13.0.0.tar.gz && \
    tar zxvf openstack-heat-13.0.0.tar.gz
RUN cd /tar/openstack-heat-13.0.0 && for p in $(ls /code/*.patch); do patch -p1 < $p; done && python setup.py install
